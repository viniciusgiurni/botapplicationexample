﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace BotApplication1.Dialogs
{
	[Serializable]
	public class NumberGuessDialog : IDialog<int>
	{
		private readonly int _number;
		private int _attempts = 0;

		public NumberGuessDialog(int number)
		{
			_number = number;
		}

		public async Task StartAsync(IDialogContext context)
		{
			await context.PostAsync($"Vamos, dê o seu primeiro palpite!");

			context.Wait(MessageReceivedAsync);
		}

		public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
		{
			var result = await activity;

			int guess;

			if (int.TryParse(result.Text, out guess))
			{
				_attempts++;

				if (guess == _number)
				{
					context.Done(_attempts);
				}
				else
				{
					if (guess < _number)
						await context.PostAsync("Errou! O número que estou pensando é MAIOR. Tente novamente.");
					else
						await context.PostAsync("Errou! O número que estou pensando é MENOR. Tente novamente.");

					context.Wait(MessageReceivedAsync);
				}
			}
			else
			{
				await context.PostAsync("Desculpe, não entendi. Poderia repetir, por favor?");
				context.Wait(MessageReceivedAsync);
			}
		}
	}
}