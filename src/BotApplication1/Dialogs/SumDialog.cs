﻿using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using Microsoft.Bot.Connector;

namespace BotApplication1.Dialogs
{
    [Serializable]
    public class SumDialog : IDialog<int>
    {
        private List<int> numbers;

        public SumDialog()
        {
            numbers = new List<int>();
        }

        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Certo, vamos somar! :)");
            await context.PostAsync("Me diga um número para começarmos.");

            context.Wait(MessageReceivedAsync);
        }

        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var message = await activity;

            int number;

            if (int.TryParse(message.Text, out number))
            {
                numbers.Add(number);
                if (numbers.Count == 1)
                    await context.PostAsync($"Ok, número {numbers.FirstOrDefault()}. Qual o próximo?");
                else
                    await context.PostAsync($"{string.Join(" + ", numbers.ToArray())}. Qual o próximo?");

                await context.PostAsync("Quando quiser o resultado, basta me dizer. :)");

                context.Wait(MessageReceivedAsync);
            }
            else
            {
                if (message.Text.ToLower().Contains("resultado"))
                {
                    await context.PostAsync($"{string.Join(" + ", numbers.ToArray())} = {numbers.Sum()}");
                    await context.PostAsync("Foi um prazer realizar esse cálculo para você!! ^__^");

                    context.Done(numbers.Sum());
                }
                else
                {
                    await context.PostAsync("Desculpe, esse número eu não entendi. :(");

                    context.Wait(MessageReceivedAsync);
                }
            }
        }

    }
}