﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace BotApplication1.Dialogs
{
	[Serializable]
	public class NameDialog : IDialog<string>
	{
		public async Task StartAsync(IDialogContext context)
		{
			await context.PostAsync("Qual é o seu nome?");

			context.Wait(MessageReceivedAsync);
		}

		public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
		{
			var name = await result;

			if (string.IsNullOrEmpty(name.Text) || name.Text.Length == 0)
			{
				await context.PostAsync("Desculpe, não entendi. Poderia repeti-lo, por favor?");

				context.Wait(MessageReceivedAsync);
			}
			else
			{
				context.Done(name.Text);
			}
		}
	}
}