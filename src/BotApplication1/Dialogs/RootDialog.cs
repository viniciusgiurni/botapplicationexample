﻿using System;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace BotApplication1.Dialogs
{
	[Serializable]
	public class RootDialog : IDialog<object>
	{
		private string name;

		public Task StartAsync(IDialogContext context)
		{

			context.Wait(MessageReceivedAsync);

			return Task.CompletedTask;
		}

		private async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
		{
			var message = await result;

            if (message.Text.ToLower().Contains("brincar"))
                await SendWelcomeMessageAsync(context);
            else if (message.Text.ToLower().Contains("somar"))
                context.Call(new SumDialog(), SumDialogResumeAfter);
            else
            {
                await context.PostAsync("Hmm, não entendi. :( Como ainda sou um filhote, por enquanto sei apenas brincar e somar. :P");
                context.Wait(MessageReceivedAsync);
            }
		}       

		private async Task SendWelcomeMessageAsync(IDialogContext context)
		{
			await context.PostAsync("Claro, vamos brincar!");

			context.Call(new NameDialog(), NameDialogResumeAfter);
		}

		private async Task NameDialogResumeAfter(IDialogContext context, IAwaitable<string> result)
		{
			name = await result;

			var numberMin = 0;
			var numberMax = 100;

			var random = new Random();
			var number = random.Next(numberMin, numberMax + 1);

			await context.PostAsync($"Certo, {name}! Vou pensar em um número aleatório entre {numberMin} e {numberMax}. Você tem que adivinhá-lo!");
			
			context.Call(new NumberGuessDialog(number), NumberGuessResumeAfter);
		}

		private async Task NumberGuessResumeAfter(IDialogContext context, IAwaitable<int> result)
		{
			var attempts = await result;

			await context.PostAsync($"Weeee, você acertou! Foram necessárias {attempts} tentativas.");

			context.Wait(MessageReceivedAsync);
		}
        private async Task SumDialogResumeAfter(IDialogContext context, IAwaitable<int> result)
        {
            await context.PostAsync("Permaneço à sua disposição, mestre. :)");
            context.Wait(MessageReceivedAsync);
        }
    }
}