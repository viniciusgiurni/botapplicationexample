﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;

namespace BotApplication1.Dialogs
{
	[Serializable]
	public class EchoDialog : IDialog<object>
	{
		protected int Count = 1;

		public async Task StartAsync(IDialogContext context)
		{
			await context.PostAsync("Bem-vindo ao método de teste! :)");
			context.Wait(MessageReceivedAsync);
		}

		public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> argument)
		{
			var message = await argument;
			if (message.Text == "reset")
			{
				PromptDialog.Confirm(context, AfterResetAsync, "Tem certeza que deseja resetar o contador?", "Desculpe, não entendi.", promptStyle: PromptStyle.None);
			}
			else
			{
				await context.PostAsync($"{Count++}: Você disse \"{message.Text}\"");
				context.Wait(MessageReceivedAsync);
			}
		}

		public async Task AfterResetAsync(IDialogContext context, IAwaitable<bool> argument)
		{
			var confirm = await argument;
			if (confirm)
			{
				Count = 1;
				await context.PostAsync("Contador resetado.");
			}
			else
			{
				await context.PostAsync("Não resetei o contador.");
			}
			context.Wait(MessageReceivedAsync);
		}
	}
}